HexGrid
=======

A simple hexagonal grid engine, supporting both two (XY) and three (RGB) axis coordinate systems.

Functionality
=============

Currently the library supports:
* Creation of a hex grid based on a number of columns and rows.
* Storage and update of arbitrary user defined data on each hex cell
* Picking (find which cell corresponds with the given screen position)
* Line segment collision (return list of cells that collide with a given screen space line)

Coordinate System
=================

The hex grid assumes that the external world is on a flat plane, thus all interaction (picking, collision) uses a 2D cartesian coordinate system. 

You can also perform all functionality using the internal hex coordinate system, which comprises of 3 axis':

<insert picture here>

Note, that this doesn't mean that the engine is '3D'! If you wanted to use this library in a 3D engine, you will need to perform the (simple) transformation from your 3D (XYZ) coordinate system to the libraries 2D (XY) system. You would then convert your 2D cartesian coordinates into the hex 3D (RGB) coordinate system.

Technology
==========
The library is currently written in Microsoft C# 3.5, though will be ported to C++ (using STL) once prototyping phase is complete with a managed C++ wrapper library.

References
==========
* Site1
* Site2
* Book1

License (New BSD License)
=======

Copyright (c) 2012, Lewis Weaver<br />
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * Neither the name of Firegem or 8thLegion nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL LEWIS WEAVER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.